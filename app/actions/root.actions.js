import _ from 'lodash'
export const ADD_ITEM = 'ADD_ITEM'
export const ADD_CONTENT = 'ADD_CONTENT'

export const addItem = (title) => ({
  type: ADD_ITEM,
  item: {
    id: _.uniqueId('todo_'),
    title: title,
    content: ''
  }
});

export const addContent = (id, content) => ({
  type: ADD_CONTENT,
  id,
  content
})
