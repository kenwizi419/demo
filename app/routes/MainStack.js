import { createStackNavigator, createAppContainer } from 'react-navigation'
import HomeScreen from '../screens/Home/Home.screen'
import ListScreen from '../screens/List/List.screen'

const AppNavigator = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen
  },
  ListScreen: {
    screen: ListScreen
  }
});

export default createAppContainer(AppNavigator)