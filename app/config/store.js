import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import { persistStore, persistReducer, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import rootReducer from '../reducers/root.reducers'

const middleware = []
if (process.env.NODE_ENV === 'development') {
    // middleware.push(logger);
}

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistCombineReducers(persistConfig, {
  root: rootReducer
})

const store = createStore(persistedReducer, applyMiddleware(...middleware));
const persistor = persistStore(store);

export {
    store,
    persistor,
};
