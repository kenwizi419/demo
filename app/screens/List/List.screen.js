import React, { PureComponent } from 'react'
import { StyleSheet, Text, View, Button, TextInput } from 'react-native'
import { connect } from 'react-redux'

import { addItem } from '../../actions/root.actions'
import TodoList from '../../components/ToDoList/TodoList.component'

class ListScreen extends PureComponent {
  constructor (props) {
    super (props)

    this.state = {
      newTitle: ''
    }
  }

  handleInput = (text) => {
    this.setState({ newTitle: text })
  }

  handleAdd = () => {
    this.props.addItem(this.state.newTitle)
    this.setState({ newTitle: '' })
  }
  
  render() {
    const { newTitle } = this.state
    console.log('Data-->', this.props.data)
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            value={newTitle}
            onChangeText={this.handleInput}
            style={styles.input}/>
          <Button
            onPress={this.handleAdd}
            title="ADD"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
            />
        </View>
        <TodoList data={this.props.data}/>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItem: (title) => {
      dispatch(addItem(title))
    }
  }
}

const mapStateToProps = (state) => ({
  data: state.root.data
})

export default connect(mapStateToProps, mapDispatchToProps)(ListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20
  },
  inputContainer: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'black',
    padding: 5
  },
  input: {
    flex: 1,
    marginRight: 10
  }
})
