import React, { PureComponent } from 'react'
import { StyleSheet, View, Button } from 'react-native'

export default class HomeScreen extends PureComponent {
  handleOpen = () => {
    this.props.navigation.navigate('ListScreen')
  }
  render() {
    return (
      <View style={styles.container}>
        <Button
          onPress={this.handleOpen}
          title="OPEN"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
