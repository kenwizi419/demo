import { ADD_ITEM, ADD_CONTENT } from '../actions/root.actions'

const initialState = {
  data: [
    {
      id: '0',
      title: 'Title 1',
      content: ''
    },
    {
      id: '1',
      title: 'Title 2',
      content: ''
    },
    {
      id: '2',
      title: 'Title 3',
      content: ''
    }
  ]
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      return {
        ...state,
        data: [...state.data, action.item]
      }
    case ADD_CONTENT:
      const data = state.data
      console.log('redux state-->', state.data)
      data[action.id].content = action.content
      console.log('State-->', data)
      return {
        ...state,
        data: data
      }
    default:
      return state
  }
};

export default reducer

