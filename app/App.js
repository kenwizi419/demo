import React, { Component } from 'react'
import { createAppContainer } from "react-navigation"
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'

import { store, persistor } from './config/store'
import AppNavigator from './routes/MainStack'
const AppContainer = createAppContainer(AppNavigator)

export default class App extends Component {
  render() {
    return <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  }
}