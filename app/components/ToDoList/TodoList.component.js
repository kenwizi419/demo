import React, { PureComponent } from 'react'
import { StyleSheet, Text, View, Button, FlatList } from 'react-native'
import PropTypes from 'prop-types'

import TodoItem from '../TodoItem/TodoItem.component'

export default class TodoList extends PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired
  }

  _keyExtractor = (item, index) => index;

  render() {
    const { data } = this.props

    return (
      <View style={styles.container}>
        <FlatList
          key={data.id}
          keyExtractor={this._keyExtractor}
          data={data}
          renderItem={({item, index}) => <TodoItem item={item} itemId={index}/>}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});
