import React, { PureComponent } from 'react'
import { StyleSheet, Text, View, Button, FlatList, TouchableOpacity, Modal, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addContent } from '../../actions/root.actions'

class TodoItem extends PureComponent {
  static propTypes = {
    item: PropTypes.object.isRequired,
    itemId: PropTypes.any
  }

  constructor (props) {
    super(props)

    const { item } = this.props

    this.state = {
      isPressed: false,
      modalVisible: false,
      content: item && item.content
    }
  }
  _keyExtractor = (item, index) => index

  handlePress = () => {
    this.setState({ isPressed: true })
  }

  hanldeLongPress = () => {
    this.setState({ modalVisible: true })
  }

  closeModal = () => {
    this.setState({ modalVisible: false })
  }

  handleInput = (text) => {
    this.setState({ content: text })
  }

  handleSubmit = () => {
    this.props.addContent(this.props.itemId, this.state.content )
    this.setState({ modalVisible: false })
  }

  render() {
    const { item } = this.props
    const buttonStyle = [styles.container]

    if (this.state.isPressed) {
      buttonStyle.push(styles.isPressed)
    }

    if (!item) {
      return null
    }
    return (
      <TouchableOpacity
        onPress={this.handlePress}
        onLongPress={this.hanldeLongPress}
        style={buttonStyle}>
        <Text style={styles.text}>{item && item.title}</Text>
        <Modal
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={this.closeModal}>
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <View style={styles.modalHeader}>
                  <Text style={styles.headerText}>{item && item.title}</Text>
              </View>
              <View style={styles.list}>
                <TextInput
                  value={this.state.content}
                  onChangeText={this.handleInput}
                  numberOfLines={10}
                  multiline
                  style={styles.input}/>
              </View>
              <View style={styles.bottomContainer}>
                  <TouchableOpacity onPress={this.handleSubmit} style={styles.button}>
                      <Text style={styles.buttonText}>OK</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.button} onPress={this.closeModal}>
                      <Text style={styles.buttonText}>Cancel</Text>
                  </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </TouchableOpacity>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addContent: (id, content) => {
      dispatch(addContent(id, content))
    }
  }
}

const mapStateToProps = (state) => ({
  data: state.root.data
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#841584',
    paddingHorizontal: 50,
    paddingVertical: 10,
    marginVertical: 5
  },
  isPressed: {
    backgroundColor: '#5084A3'
  },
  text: {
    fontSize: 15,
    color: 'white',
    textAlign: 'center',
  },
  modal: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '60%',
    height: '80%',
    backgroundColor: 'rgba(255,255,255,0.5)'
  },
  input: {
    alignItems: 'flex-start',
    width: '100%',
    height: '100%'
  },
  modalContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.6)'
  },
  modalContent: {
    width: 300, 
    borderColor: '#841584',
    borderWidth: 2, 
    backgroundColor: 'white', 
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  modalHeader: {
    height: 30,
    paddingVertical: 5,
    backgroundColor: '#841584',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerText: {
    color: 'white',
    fontSize: 17,
  },
  list: {
    height: 280,
    padding: 15
  },
  bottomContainer: {
    height: 30,
    paddingVertical: 5,
    flexDirection: 'row',
    backgroundColor: '#841584',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
  }
})

